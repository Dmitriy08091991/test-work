
import tabs from './modules/tabs';
import quiz from "./modules/quiz/quiz";


window.addEventListener('DOMContentLoaded', () => {
    "use strict";

    const DATA = [
        {
            question: "What is 10/2?",
            answers: [
                {
                    id: 1,
                    value: 5,
                },
                {
                    id: 2,
                    value: 30,
                },
                {
                    id: 3,
                    value: 10,
                },
            ],
            correct: 1
        },
        {
            question: "What is 30/2?",
            answers: [
                {
                    id: 4,
                    value: 5,
                },
                {
                    id: 5,
                    value: 15,
                },
                {
                    id: 6,
                    value: 10,
                },
            ],
            correct: 2
        },
        {
            question: "What is 40/2?",
            answers: [
                {
                    id: 7,
                    value: 5,
                },
                {
                    id: 8,
                    value: 15,
                },
                {
                    id: 9,
                    value: 20,
                },
            ],
            correct: 9
        },
        {
            question: "What is 50/2?",
            answers: [
                {
                    id: 10,
                    value: 5,
                },
                {
                    id: 11,
                    value: 25,
                },
                {
                    id: 12,
                    value: 10,
                },
            ],
            correct: 11
        },
        {
            question: "What is 60/2?",
            answers: [
                {
                    id: 13,
                    value: 5,
                },
                {
                    id: 14,
                    value: 15,
                },
                {
                    id: 15,
                    value: 30,
                },
            ],
            correct: 15
        },
    ]

    tabs('.icon-box-tabs', '.icon-box_icon', '.icon-box-tabs .img-wrap > img', 'active', 'inline-block');

    quiz(
        '#quiz',
        '#questions',
        '#progress',
        '#result',
        '#btn-next',
        'btn-prev',
        DATA
    )

});
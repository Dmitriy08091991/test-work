import {quizItem} from "./template";

const quiz = (quizWrap, quizQuestions, quizProgress, quizResult, quizNext, quizPrev, data) => {
    const quiz = document.querySelector(quizWrap);
    const questions = document.querySelector(quizQuestions);
    const progress = document.querySelector(quizProgress);
    const results = document.querySelector(quizResult);
    const btnNext = document.querySelector(quizNext);
    const btnPrev = document.querySelector(quizPrev);

    let resultData = {}
    let correctData = {}

    const renderQuestions = (index) => {
        progress.innerHTML = ''
        renderProgress(index + 1);

        questions.dataset.currentStep = index;

        const renderAnswers = () => {
            return data[index].answers.map(answer => {
                return `<li>
                        <input id="${answer.id}" class="answer-input" type="radio" name="${index}" value="${answer.id}">
                        <label for="${answer.id}">
                               ${answer.value}
                        </label>
                    </li>`
            })
                .join('');
        }

        questions.innerHTML = `
        <div class="quiz-questions-item">
            <div class="quiz-questions__question">
                <h4 class="text-center">${data[index].question}</h4>
            </div>
            <ol class="quiz-questions__answers">
                ${renderAnswers()}
            </ol>
        </div>
        `
    }

    const renderResult = () => {
        let content = '';
        const correct = [];
        const result = [];
        Object.keys(correctData).map(item => {
            correct.push(correctData[item])
        }).join('');

        Object.keys(resultData).map(item => {
            result.push(resultData[item])
        }).join('');
        for (let i = 0; i < correct.length; i++) {
            for (let j = 0; j < result.length; j++) {
                if (result[j] !== correct[i]) {
                    content = 'foooooo'
                }else{
                    content = 'great'
                }
            }
        }
        results.innerHTML = content
    }

    const renderProgress = (currentStep) => {
        let activeClass = '';
        data.forEach((_, index) => {
            if (currentStep === index + 1) {
                activeClass = 'active'
            } else {
                activeClass = ''
            }
            progress.insertAdjacentHTML('beforeend', `<div><span class="progress__dot ${activeClass}">${index + 1}</span><span>Question ${index + 1}</span></div>`)
        })
    }

    quiz.addEventListener('change', (event) => {
        if (event.target.classList.contains('answer-input')) {
            resultData[event.target.name] = Number(event.target.value);
            correctData[event.target.name] = data[event.target.name].correct
            let correctAnswer = Number(data[event.target.name].correct);
            if (correctAnswer === Number(event.target.value)) {
                event.target.parentNode.classList.add('correct')
                console.log("CORRECT")
            } else {
                event.target.parentNode.classList.add('wrong')
                console.log('WRONG')

            }
            btnNext.disabled = false;
        }
    })

    quiz.addEventListener('click', (event) => {
        if (event.target.classList.contains('btn-next')) {
            const nextQuestionIndex = Number(questions.dataset.currentStep) + 1;

            if (data.length === nextQuestionIndex) {
                questions.innerHTML = '';
                progress.innerHTML = ''
                renderResult()
            } else {
                renderQuestions(nextQuestionIndex);
            }
            btnNext.disabled = true

        } else if (event.target.classList.contains('btn-prev')) {
            const nextQuestionIndex = Number(questions.dataset.currentStep) - 1;


            if (data.length === nextQuestionIndex) {
                renderResult()
            } else {
                renderQuestions(nextQuestionIndex);
            }
            btnNext.disabled = true
        }
    })

    renderQuestions(0);
}

export default quiz;